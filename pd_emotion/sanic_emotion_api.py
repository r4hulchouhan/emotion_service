
import sys
from sanic import Sanic
from sanic.response import  json
from sanic import response
import translate
from sanic_sentry import SanicSentry
from sanic_cors import CORS, cross_origin
import traceback
import settings
import security
import logging
import json
import math
import DeepMoji
import ast
from goose3 import Goose
import sanic_limiter_func
import datetime
#from trained.new_predict import get_emotion
app = Sanic(__name__)

app.config['SENTRY_DSN'] = "https://b4b3b106a12346fd8b9cf719b8a04150:3e34063899cc4b80899a5907a955b6be@sentry.io/1199764"

plugin = SanicSentry()
plugin.init_app(app)

CORS(app)

g=Goose()

def emotion_call( text ):
        #print(text[0])
        resp	= DeepMoji.emotion_classify( text[0] )
        #resp=get_emotion(text[0])
        return resp

def emotion_call_v3( text ):
	#print(text[0])
	resp		= DeepMoji.emotion_classify_v3( text[0] )
	output          = resp
	#print(output)
	return output

@app.route("/v4/emotion",methods= ['POST'])
async def emotion(request):
	api_type     = "emotion"
	real_ip      = request.headers.get( "X-Real-Ip" )
	headers      = request.headers
	source       = request.headers.get( "source", None )
	user_agent   = headers.get("User-Agent",None  )
	lang_code    = request.form.get( "lang_code", "en")
	api_key      = request.form.get( "api_key", None )
	text         = request.form.get( "text", None)
	if text :
		pass
	else:
		url = request.form.get("url", None)
		if url:
			text=g.extract(url=url)
			text=text.cleaned_text

	if api_key == None or api_key == "" or text == None or type( text ) == None or text == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)
	
	

	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)


	if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
		resp = {"code":1}
	else:
		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)
	if resp['code'] == 403:
		return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English."})

	if resp['code'] == 429:
		return response.json({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})
	
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})

	

	if lang_code == None or type( lang_code ) == None:
		output = settings.error_param_lang
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)


	weights     = math.ceil( len( text ) / settings.size )



	if lang_code == "en":
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, emotion_call, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= "en" )
		else:
			output = security.validate_and_execute( api_key, api_type, weights, emotion_call, text )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= "en" )
		return response.json(output)

	elif lang_code not in settings.iso_language_codes:
		output = settings.error_lang_codes
		security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		return response.json(output)
	
	else :
		text   = translate.azure_translate( text, lang_code )
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, emotion_call, text )
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		else:
			output = security.validate_and_execute( api_key, api_type, weights, emotion_call, text )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		return response.json(output)

@app.route("/v4/emotion_batch",methods= ['POST'])
async def emotion_batch(request):
	api_type    = "emotion"
	real_ip     = request.headers.get( "X-Real-Ip" )
	headers     = request.headers
	source      = headers.get( "Source", None )

	api_key     = request.form.get( "api_key", None )
	lang_code   = request.form.get( "lang_code", "en")
	data 		= request.form.get("text", None)

	if api_key == None or api_key == "" or data == None or type( data ) == None or data == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)

	# if data == None or type( data ) == None or data == "":
	# 	output = settings.error_param_text
	# 	security.register( "basic", real_ip, headers, api_type, api_key, None, output, data= data )
	# 	return response.json(output)

	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)

	resp=sanic_limiter_func.check_paid_user(api_key)
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})
	
	#status = security.check_api_key_blocked(api_key)
	#if status == True:
	#	output = {"code":401,"Error":"Your API KEY is blocked"}
	#	security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
	#	return response.json(output)
	

		
	try:
		data    = ast.literal_eval(data)
	except:
		output = settings.error_invalid_input_format
		return response.json(output)

	if type(data) is type(list()) or type(data) is type(tuple()):
		pass
	else:
		output = settings.error_invalid_input_format
		return response.json(output)

	return_array = []
	for text in data:
		weights = math.ceil( len( text ) / settings.size )
		if lang_code not in settings.iso_language_codes:
			output = settings.error_lang_codes
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			return response.json(output)

		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)
		if resp['code'] == 403:
			return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English."})
		
		if resp['code'] == 400:
			return response.json({"code":400,"Error":"Invalid API Key"})

		if resp['code'] == 429:
			return_array.append({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})

		else:
		
			try:
				if lang_code == "en":
					output  = security.validate_and_execute( api_key, api_type, weights, emotion_call, text )
					security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text )
				else:
					text   = translate.azure_translate( text, lang_code )
					output = security.validate_and_execute( api_key, api_type, weights, emotion_call, text )
					security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			except:
				output  = None
				logging.error( "Error <500>: %s"%traceback.format_exc() )
				
			if output is not None:
				if 'Error' in output:
					return response.json(output)
				else:
					return_array.append( output['emotion'] )
			else:
				return_array.append( {} )	
	output = { "emotion": return_array}
		
	return response.json(output)

@app.route("/v3/emotion",methods= ['POST'])
async def emotion(request):
	api_type     = "emotion"
	real_ip      = request.headers.get( "X-Real-Ip" )
	headers      = request.headers
	source       = request.headers.get( "source", None )
	user_agent   = headers.get("User-Agent",None  )
	lang_code    = request.form.get( "lang_code", None)
	if lang_code == None:
		lang_code = request.args.get("lang_code",None)
	if lang_code == None:
		lang_code = "en"
	api_key      = request.form.get( "api_key", None )
	if api_key == None:
		api_key = request.args.get("api_key",None)
	text         = request.form.get( "text", None)
	if text == None:
		text = request.args.get("text",None)
	if text :
		pass
	else:
		url = request.form.get("url", None)
		if url:
			text=g.extract(url=url)
			text=text.cleaned_text

	if api_key == None or api_key == "" or text == None or type( text ) == None or text == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)
	
	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)

	if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
		resp = {"code":1}
	else:
		# print("free")
		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)

	if resp['code'] == 403:
		return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English."})
	
	if resp['code'] == 429:
		return response.json({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})
	
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})

	

	if lang_code == None or type( lang_code ) == None:
		output = settings.error_param_lang
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)


	weights     = math.ceil( len( text ) / settings.size )


	if lang_code == "en":
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, emotion_call_v3, text )
			output['code'] = 200
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= "en" )
		else:
			output = security.validate_and_execute( api_key, api_type, weights, emotion_call_v3, text )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= "en" )
			output['code'] = 200
		return response.json(output)

	elif lang_code not in settings.iso_language_codes:
		output = settings.error_lang_codes
		security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		return response.json(output)
	
	else :
		text   = translate.azure_translate( text, lang_code )
		if source in settings.excel_headers or request.headers.get( settings.excel_source_version, None ) or request.headers.get( settings.excel_source_os_version, None )!= None:
			output = security.excel_validate_and_execute( api_key, api_type, weights, emotion_call_v3, text )
			output['code'] = 200
			security.register( "excel", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		else:
			output = security.validate_and_execute( api_key, api_type, weights, emotion_call_v3, text )
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			output['code'] = 200
		return response.json(output)

@app.route("/v3/emotion_batch",methods= ['POST'])
async def emotion_batch(request):
	api_type    = "emotion"
	real_ip     = request.headers.get( "X-Real-Ip" )
	headers     = request.headers
	source      = headers.get( "Source", None )
	lang_code   = request.form.get( "lang_code", None)
	if lang_code == None:
		lang_code = request.args.get("lang_code",None)
	if lang_code == None:
		lang_code = "en"
	api_key      = request.form.get( "api_key", None )
	if api_key == None:
		api_key = request.args.get("api_key",None)
	data         = request.form.get( "text", None)
	if data == None:
		data = request.args.get("text",None)

	if api_key == None or api_key == "" or data == None or type( data ) == None or data == "":
		output = {"code":400,"Error":"Parameter `API Key`  or `TEXT` is either not specified or set to null."}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= data )
		return response.json(output)


	status = security.check_api_key_blocked(api_key)
	if status == True:
		output = {"code":401,"Error":"Your API KEY is blocked"}
		security.register( "basic", real_ip, headers, api_type, api_key, None, output, text= text )
		return response.json(output)


	resp=sanic_limiter_func.check_paid_user(api_key)
	if resp['code'] == 400:
		return response.json({"code":400,"Error":"Invalid API Key"})
	

	try:
		data    = ast.literal_eval(data)
	except:
		output = settings.error_invalid_input_format
		return response.json(output)

	if type(data) is type(list()) or type(data) is type(tuple()):
		pass
	else:
		output = settings.error_invalid_input_format
		return response.json(output)

	return_array = []
	for text in data:
		weights = math.ceil( len( text ) / settings.size )
		if lang_code not in settings.iso_language_codes:
			output = settings.error_lang_codes
			security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
			return response.json(output)

		status = security.check_api_key_rate_limit_restriction(api_key)
		if status == True:
			resp = {"code":1}
		else:
			resp=sanic_limiter_func.check_paid_user(api_key,lang_code)

		if resp['code'] == 403:
			return response.json({"code":403,"Error":"You are currently subscribed to the "+resp['package_name']+" plan. Please upgrade your account from your dashboard to use ParallelDots APIs in languages other than English."})
		
		if resp['code'] == 429:
			return response.json({"code":429,"Error":"Oops! You have exceeded the rate limit. Please upgrade your plan to get higher rate limits - https://user.apis.paralleldots.com/user_dashboard."})
		
		if resp['code'] == 400:
			return response.json({"code":400,"Error":"Invalid API Key"})


		
		try:
			if lang_code == "en":
				output  = security.validate_and_execute( api_key, api_type, weights, emotion_call_v3, text )
				security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text )
			else:
				text   = translate.azure_translate( text, lang_code )
				output = security.validate_and_execute( api_key, api_type, weights, emotion_call_v3, text )
				security.register( "basic", real_ip, headers, api_type, api_key, weights, output, text= text, lang_code= lang_code )
		except:
			output  = None
			logging.error( "Error <500>: %s"%traceback.format_exc() )
			
		if output is not None:
			if 'Error' in output:
				return response.json(output)
			else:
				return_array.append( output['emotion']['probabilities'] )
		else:
			return_array.append( {} )	
	output = { "emotion": return_array}
	output['code'] = 200
	return response.json(output)

if __name__ == "__main__":
	app.run(host="0.0.0.0",port=9217)
