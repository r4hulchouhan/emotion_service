#!/usr/bin/python

__author__ = "Manish Kumar"

# All Settings and Configurations for the Servers
apis = [ "sentiment", "ner", "taxonomy", "similarity", "keywords", "intent", "emotion", "abuse", "multilang_keywords", "popularity", "custom_classifier", "text_parser", "nsfw", "phrase_extractor" ]

size = 600.0

excel_daily_quota			= 2000
excel_monthly_quota			= 60000

daily_usage_quota			= 2000
monthly_usage_quota			= 60000

visual_daily_quota			= 2000
visual_monthly_quota		= 60000

pricing_unit				= 1000	# For every pricing_unit the pricing_cost is charged
pricing_cost				= 1		# Value in USD ( N.B. Must be multiplied by 100 when billing to reflect value of Dollar in Cents )
pricing_cost_1M				= 0.8

visual_pricing_unit			= 100	# For every visual_pricing_unit the pricing_cost is charged
visual_pricing_cost			= 1		# Value in USD ( N.B. Must be multiplied by 100 when billing to reflect value of Dollar in Cents )
visual_pricing_cost_100K	= 0.8

premium_monthly_quota		= 100
premium_pricing_unit		= 1000
premium_pricing_cost		= 2
premium_pricing_cost_1M		= 1.75

custom_classifier_quota		= 100
custom_pricing_unit			= 1000
custom_pricing_cost			= 3
custom_pricing_cost_1M		= 2.75
custom_pricing_cost_10M		= 2.5

excel_pricing_unit			= 1000	# For every pricing_unit the pricing_cost is charged
excel_pricing_cost			= 5		# Value in USD ( N.B. Must be multiplied by 100 when billing to reflect value of Dollar in Cents )
excel_pricing_cost_50K		= 3
excel_pricing_cost_500K		= 1.5

path						= "/home/ankit/"
demo_path					= "/datadrive/"
image_upload_root			= "/datadrive/visual_api_images/%s"
general_log_path			= "/datadrive/logs/"
consumer_log_path			= "/datadrive/logs/consumer/"

rabbit_mq_parameters		= "amqp://meghdeep:balsala2358@mq.paralleldots.com:5672/%2F"
#rabbit_mq_parameters		= "amqp://oxnglsqs:i5EPh2uNslKMw7MKsDz-1ZRky78bYuHU@black-boar.rmq.cloudamqp.com/oxnglsqs"

redis_parameters			= "redis1442198.paralleldots.com"
#redis_parameters                        = "52.146.38.30"
#sentry_credentials			= "https://e902197b6b7e4e65ae85e7e37824674d:86048c0f28ab47a4bc11b38e4489b38a@sentry.io/1191272"	# Dev
sentry_credentials			= "https://b4b3b106a12346fd8b9cf719b8a04150:3e34063899cc4b80899a5907a955b6be@sentry.io/1199764"	# Prod

loader_io_credentials		= "loaderio-c0a6ba27725a738e19da672dfd324711"


excel_user_agent            = ["RestSharp 103.1.0.0", "RestSharp/105.2.3.0", "RestSharp/106.1.0.0", "RestSharp/106.2.1.0"]
excel_headers				= [ "MSExcel", "GoogleSheets"]
excel_source_version		= "Source-Excel-Version"
excel_source_os_version     = "Source-Os-Version"
visual_mimetypes			= [ "image/jpeg", "image/png" ]

multilang_apis				= [ "pt", "zh", "es" ]
iso_language_codes			= [ "en", "pt", "zh", "es", "de", "fr", "nl", "it", "ja", "th", "da", "fi", "el", "ru", "ar", "no"]

error_api_key				= { "Error": "Invalid Credentials. Please provide valid API key."                                                                                          , "code": 401 }
error_invalid_input			= { "Error": "Invalid Format. Please provide valid input parameters."                                                                                      , "code": 406 }
error_invalid_input_format  = { "Error": "Invalid Format. Text should be a list or a tuple not a string"                                                                                            , "code": 406 }
error_500					= { "Error": "Backend error. Please email at support@paralleldots.com to get this error resolved on priority."                                                     , "code": 500 }
error_premium_apis			= { "Error": "Please upgrade your account from the dashboard to access this API. You can upgrade at https://user.apis.paralleldots.com/user_dashboard"     , "code": 403 }
error_basic_daily_quota		= { "Error": "Daily Limit Exceeded. Please upgrade your account from your user dashboard at https://user.apis.paralleldots.com/user_dashboard"             , "code": 403 }
error_basic_monthly_quota	= { "Error": "Monthy Limit Exceeded. Please upgrade your account from your user dashboard at https://user.apis.paralleldots.com/user_dashboard"            , "code": 403 }
error_visual_daily_quota	= { "Error": "Visual API Daily Limit Exceeded. Please upgrade your account from your user dashboard at https://user.apis.paralleldots.com/user_dashboard"  , "code": 403 }
error_visual_monthly_quota	= { "Error": "Visual API Monthly Limit Exceeded. Please upgrade your account from your user dashboard at https://user.apis.paralleldots.com/user_dashboard", "code": 403 }
error_excel_daily_quota		= { "Error": "Excel Daily Limit Exceeded. Please upgrade your account from your , dashboard at https://user.apis.paralleldots.com/user_dashboard"          , "code": 403 }
error_excel_monthly_quota	= { "Error": "Excel Monthy Limit Exceeded. Please upgrade your account from your user dashboard at https://user.apis.paralleldots.com/user_dashboard"      , "code": 403 }
error_bad_url				= { "Error": "Error reading Image from URL. Please try some other URL."                                                                                    , "code": 400 }
error_invalid_visual		= { "Error": "No Input image uploaded or Input URL given."                                                                                                 , "code": 400 }
error_bad_text				= { "Error": "Invalid Format. Parameter `text` should be string."                                                                                          , "code": 400 }
error_bad_lang				= { "Error": "Invalid Format. Parameter `lang_code` should be string."                                                                                     , "code": 400 }
error_param_text			= { "Error": "Bad Request. Please provide valid input parameter. Valid values - [ `text`, `api_key` ]."                                                    , "code": 400 }
error_param_lang			= { "Error": "Bad Request. Please provide valid input parameter. Valid values - [ `text`, `api_key`, `lang_code` ]."                                       , "code": 400 }
error_lang_codes			= { "Error": "The lang_code is not among the supported languages, supported languages: %s."%", ".join( iso_language_codes )                                , "code": 400 }


# Port Numbers for Tornado Servers
tornado_port_sentiment			= 8030
tornado_port_emotion			= 8031
tornado_port_ner				= 8032
tornado_port_taxonomy			= 8033
tornado_port_similarity			= 8034
tornado_port_keywords			= 8035
tornado_port_intent				= 8036
tornado_port_abuse				= 8037
tornado_port_multilang			= 8038
tornado_port_sentiment_social	= 8039
tornado_port_tota				= 8040
tornado_port_popularity			= 8041
tornado_port_parser				= 8042
tornado_port_nsfw				= 8043
tornado_port_tota_v3			= 8044
tornado_port_phrase_extractor	= 8045
tornado_port_tkg				= 8046
tornado_port_facial_emotion		= 8047
tornado_port_object_recognizer	= 8048
tornado_port_lang_dect			= 8049
tornado_port_similarity_demo	= 8050

# Port for the Demos
port_website_demo				= 8070
port_url_extract				= 8075
port_batch_process				= 8072
port_pblish_categories 			= 5010

port_usage_reset				= 8090
port_usage						= 8091
port_utilities					= 8092

port_translate					= 8100
port_loader_io					= 8101

