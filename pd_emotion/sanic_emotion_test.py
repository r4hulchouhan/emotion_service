from sanic_emotion_api import app
import json
text    = "Prime Minister Narendra Modi tweeted a link to the speech Human Resource Development Minister Smriti Irani made in the Lok Sabha during the debate on the ongoing JNU row and the suicide of Dalit scholar Rohith Vemula at the Hyderabad Central University."

batch_text	= json.dumps(["Apple was founded by Steve Jobs.","Apple Inc. is an American multinational technology company headquartered in Cupertino, California"])

api_key = "sU6x7TOjTcIUPdSY7tUbbB3v38ViKeQw2RrPkiXjEgw"

JSON_DATA= {"emotion":{"Sad":0.0794725214,"Excited":0.2455485924,"Angry":0.2028275603,"Bored":0.0393167888,"Happy":0.3121236311,"Fear":0.1207109061}}

BATCH_JSON_DATA= {"emotion":[{"Sad":0.1886738055,"Excited":0.2585116496,"Angry":0.1146940451,"Bored":0.1184973466,"Happy":0.1186171572,"Fear":0.2010059961},{"Sad":0.0577940163,"Excited":0.4376697564,"Angry":0.052839071,"Bored":0.0,"Happy":0.3037971619,"Fear":0.1478999944}]}

def test_emotion():
        request, response = app.test_client.post('/v4/emotion',data={ "text": text, "api_key": api_key })
        assert type(response.json) is dict
        assert response.status == 200
        assert response.json == JSON_DATA
def test_emotion_batch():
        request, response = app.test_client.post('/v4/emotion_batch',data={ "text": batch_text, "api_key": api_key })
        assert type(response.json) is dict
        assert response.status == 200
        assert response.json == BATCH_JSON_DATA
test_emotion()
test_emotion_batch()
