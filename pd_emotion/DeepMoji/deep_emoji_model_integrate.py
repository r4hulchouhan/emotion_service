# -*- coding: utf-8 -*-

""" Use DeepMoji to score texts for emoji distribution.

The resulting emoji ids (0-63) correspond to the mapping
in emoji_overview.png file at the root of the DeepMoji repo.

Writes the result to a csv file.
"""
from __future__ import print_function, division
from .example_helper import *
import json
import csv,os
import numpy as np
from .deepmoji.sentence_tokenizer import SentenceTokenizer
from .deepmoji.model_def import deepmoji_emojis
from .deepmoji.global_variables import PRETRAINED_PATH, VOCAB_PATH
from operator import itemgetter
#from tornado import ioloop,web
import numpy as np



emoji_dict = {"0"	:"Excited","1"	:"Angry","2"	:"Sad","3"	:"Sad","4"	:"Happy","5"	:"Sad","6"	:"Happy","7"	:"Happy","8":"Happy","9"	:"Happy","10"	:"Fear","11"	:"Happy","12"	:"Fear","13"	:"Happy","14"	:"Bored","15"	:"Happy","16"	:"Happy","17"	:"Excited","18"	:"Happy","19"	:"Sad","20"	:"Fear","21"	:"Happy","22"	:"Sad","23"	:"Happy","24"	:"Happy","25"	:"Sad","26"	:"Happy","27"	:"Sad","28"	:"Fear","29"	:"Sad","30"	:"Happy","31"	:"Excited","32"	:"Angry","33"	:"Happy","34"	:"Sad","35"	:"Sad","36"	:"Happy","37"	:"Angry","38"	:"Angry","39"	:"Fear","40"	:"Happy","41"	:"Fear","42"	:"Angry","43"	:"Fear","44"	:"Excited","45"	:"Fear","46"	:"Sad","47"	:"Happy","48"	:"Happy","49"	:"Fear","50"	:"Happy","51"	:"Fear","52"	:"Sad","53"	:"Happy","54"	:"Happy","55"	:"Angry","56"	:"Angry","57"	:"Excited","58"	:"Angry","59"	:"Happy","60"	:"Happy","61"	:"Happy","62"	:"Fear","63"	:"Excited"}
# emoji_dict = { "0": "Excited", "1": "Sad", "2": "Sad", "3": "Sad", "4": "Happy", "5": "Sad", "6": "Excited", "7": "Excited", "8": "Happy", "9": "Sarcasm", "10": "Happy", "11": "Happy", "12": "Fear", "13": "Happy", "14": "Bored", "15": "Happy", "16": "Happy", "17": "Excited", "18": "Happy", "19": "Sarcasm", "20": "Happy", "21": "Happy", "22": "Sad", "23": "Happy", "24": "Happy", "25": "Sarcasm", "26": "Sarcasm", "27": "Sad", "28": "Happy", "29": "Sad", "30": "Happy", "31": "Excited", "32": "Angry", "33": "Happy", "34": "Sad", "35": "Sad", "36": "Happy", "37": "Angry", "38": "Happy", "39": "Angry", "40": "Excited", "41": "Fear", "42": "Fear", "43": "Sad", "44": "Fear", "45": "Sad", "46": "Sad", "47": "Happy", "48": "Happy", "49": "Happy", "50": "Happy", "51": "Fear", "52": "Sad", "53": "Excited", "54": "Happy", "55": "Angry", "56": "Angry", "57": "Happy", "58": "Angry", "59": "Happy", "60": "Happy", "61": "Happy", "62": "Sarcasm", "63": "Excited" }


def top_elements(array, k):
	ind = np.argpartition(array, -k)[-k:]
	return ind[np.argsort(array[ind])][::-1]


maxlen = 30
batch_size = 32


with open(VOCAB_PATH, 'r') as f:
	vocabulary = json.load(f)

st = SentenceTokenizer(vocabulary, maxlen)


model = deepmoji_emojis(maxlen, PRETRAINED_PATH)
model.summary()

def deepEmojiModelIntegration(TEST_SENTENCES):
	# TEST_SENTENCES = unicode(str(TEST_SENTENCES),"utf-8")
	#print(TEST_SENTENCES)
	tokenized, _, _ = st.tokenize_sentences(TEST_SENTENCES)
	#print(tokenized)
	prob = model.predict(np.array(tokenized))
	scores = []
	for i, t in enumerate(TEST_SENTENCES):
		t_tokens = tokenized[i]
		t_score = [t]
		t_prob = prob[i]
		ind_top = top_elements(t_prob, 63)
		t_score.append(sum(t_prob[ind_top]))
		t_score.extend(ind_top)
		t_score.extend([t_prob[ind] for ind in ind_top])
		scores.append(t_score)
		resp = scores[0]
		return scores


def emotion_classify(requesting_sentence):
	requesting_sentence = requesting_sentence
	#print("dw",requesting_sentence)
	resp = deepEmojiModelIntegration([requesting_sentence])[0]
	#print(resp)
	remaining_total = 100-(resp[1]*100)
	remaining_total = remaining_total/(resp[1]*100)
		
	temp_emoji_list = []
	
	for i in range(2,65):
		#print(resp[i])
		emoji_word = emoji_dict[str(resp[i])]
		score      = resp[i+63]
		flag = 0
		for obj in temp_emoji_list:
			if emoji_word == obj["word"]:
				flag = 1
				obj["count"] = obj["count"]+1
				obj["score"] = obj["score"]+score
				obj["avg"] = obj["score"]*1.0/obj["count"]*1.0
		if flag == 0:
			temp_emoji_list.append({"avg":score,"word":emoji_word,"count":1,"score":score})	
	#print(temp_emoji_list)
	total = 0
	for i in temp_emoji_list:
		total = i["avg"] + total

	remaining_total = 100-(total*100)
	remaining_total = remaining_total/(total*100)
	emoji_exp = sorted(temp_emoji_list, key=itemgetter('avg'),reverse=True)[0]["word"]		
	final_out = {}
	# final_out["emotion"]={}
	final_out["emotion"] = emoji_exp
	out = {}
	un_list = set(emoji_dict.values())
	for j in un_list:
		out[j] = float(0.0)
	for i in temp_emoji_list:
		i["score"] = i["score"] *100
		out[i["word"]] = (i["avg"]*remaining_total+i["avg"]).astype(np.float64)
	# final_out["probabilities"] = out 
	output = {}
	output["emotion"] = out
	#print(output)
	return output


def emotion_classify_v3(requesting_sentence):
	requesting_sentence = requesting_sentence
	#print("dw",requesting_sentence)
	resp = deepEmojiModelIntegration([requesting_sentence])[0]
	#print(resp)
	remaining_total = 100-(resp[1]*100)
	remaining_total = remaining_total/(resp[1]*100)
		
	temp_emoji_list = []
	
	for i in range(2,65):
		#print(resp[i])
		emoji_word = emoji_dict[str(resp[i])]
		score      = resp[i+63]
		flag = 0
		for obj in temp_emoji_list:
			if emoji_word == obj["word"]:
				flag = 1
				obj["count"] = obj["count"]+1
				obj["score"] = obj["score"]+score
				obj["avg"] = obj["score"]*1.0/obj["count"]*1.0
		if flag == 0:
			temp_emoji_list.append({"avg":score,"word":emoji_word,"count":1,"score":score})	
	#print(temp_emoji_list)
	total = 0
	for i in temp_emoji_list:
		total = i["avg"] + total

	remaining_total = 100-(total*100)
	remaining_total = remaining_total/(total*100)
	emoji_exp = sorted(temp_emoji_list, key=itemgetter('avg'),reverse=True)[0]["word"]		
	final_out = {}
	# final_out["emotion"]={}
	final_out["emotion"] = emoji_exp
	out = {}
	un_list = set(emoji_dict.values())
	for j in un_list:
		out[j] = float(0.0)
	for i in temp_emoji_list:
		i["score"] = i["score"] *100
		out[i["word"]] = (i["avg"]*remaining_total+i["avg"]).astype(np.float64)
	final_out["probabilities"] = out 

	#print(output)
	return final_out

# class DeepEmojiModelIntegrationClass(web.RequestHandler):
# 	def post(self):
# 		self.set_header('Access-Control-Allow-Origin', '*')
# 		self.set_header('Access-Control-Allow-Credentials', 'true')
# 		self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
# 		self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
		
# 		requesting_sentence = self.get_argument("sentence",None)

# 		if requesting_sentence is None or not requesting_sentence:
# 			self.set_header("Content-Type", "application/json")
# 			self.set_status(200)
# 			self.finish(json.dumps({'status':0,'msg':"Please provide me sentence"}))
# 			return

# 		# print (requesting_sentence)
# 		# print (type(requesting_sentence))
# 		requesting_sentence = unicode(str(requesting_sentence),"utf-8")
# 		resp = deepEmojiModelIntegration([requesting_sentence])[0]

# 		temp_emoji_list = []

# 		for i in range(2,7):
# 			emoji_word = emoji_dict[str(resp[i])]
# 			score      = resp[i+5]

# 			flag = 0
# 			for obj in temp_emoji_list:
# 				if emoji_word == obj["word"]:
# 					flag = 1
# 					obj["count"] = obj["count"]+1
# 					obj["score"] = obj["score"]+score
# 					obj["avg"] = obj["score"]*1.0/obj["count"]*1.0
# 			if flag == 0:
# 				temp_emoji_list.append({"avg":score,"word":emoji_word,"count":1,"score":score})	

# 		print(temp_emoji_list)

# 		emoji_exp = sorted(temp_emoji_list, key=itemgetter('avg'),reverse=True)[0]["word"]
		
# 		final_out = {}
# 		# final_out["emotion"]={}
# 		final_out["emotion"] = emoji_exp
# 		out = {}
# 		un_list = set(emoji_dict.values())
# 		for j in un_list:
# 			out[j] = 0.0
# 		for i in temp_emoji_list:
# 			out[i["word"]] = i["avg"].astype(np.float64)

# 		final_out["probabilities"] = out 

		
# 		self.set_header("Content-Type", "application/json")
# 		self.set_status(200)
# 		self.finish(json.dumps(final_out))
# 		return

# settings = {
#     "template_path": os.path.join(os.path.dirname(__file__), "templates"),
#     "static_path": os.path.join(os.path.dirname(__file__), "static"),
#     "debug" : True
# }

# application = web.Application([
# 	(r'/emotion', DeepEmojiModelIntegrationClass),
# ],**settings)

# if __name__ == "__main__":
# 	print ("Here we go")
# 	application.listen(8890)
# 	ioloop.IOLoop.instance().start()

#print (emotion_classify(["My girlfriend is always stealing my t-shirts and sweaters but if I take one of her dresses suddenly we need to talk"]))
