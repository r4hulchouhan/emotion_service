""" Global variables.
"""
import tempfile
import os,sys
from google.cloud import storage
import configparser
config = configparser.ConfigParser()

config.read('path.ini')

model_path=config['MODEL']['PATH']
# The ordering of these special tokens matter
# blank tokens can be used for new purposes
# Tokenizer should be updated if special token prefix is changed
SPECIAL_PREFIX = 'CUSTOM_'
SPECIAL_TOKENS = ['CUSTOM_MASK',
                  'CUSTOM_UNKNOWN',
                  'CUSTOM_AT',
                  'CUSTOM_URL',
                  'CUSTOM_NUMBER',
                  'CUSTOM_BREAK']
SPECIAL_TOKENS.extend(['{}BLANK_{}'.format(SPECIAL_PREFIX, i) for i in range(6, 10)])

# ROOT_PATH = dirname(dirname(abspath(__file__)))
#PRETRAINED_PATH = '{}/model/deepmoji_weights.hdf5'.format(ROOT_PATH)
if os.path.isfile(model_path+"vocabulary.json")==True:
  VOCAB_PATH = model_path+"vocabulary.json"
else:
  client = storage.Client()
  bucket = client.get_bucket('apis_models')
  blob = bucket.blob('emotion/vocabulary.json')
  blob.download_to_filename(model_path+'vocabulary.json')
  VOCAB_PATH = model_path+"vocabulary.json"
if os.path.isfile(model_path+"deepmoji_weights.hdf5")==True:
  PRETRAINED_PATH = model_path+"deepmoji_weights.hdf5" 
else:
  client = storage.Client()
  bucket = client.get_bucket('apis_models')
  blob = bucket.blob('emotion/deepmoji_weights.hdf5')
  blob.download_to_filename(model_path+'deepmoji_weights.hdf5')
  PRETRAINED_PATH = model_path+"deepmoji_weights.hdf5"
WEIGHTS_DIR = tempfile.mkdtemp()

NB_TOKENS = 50000
NB_EMOJI_CLASSES = 64
FINETUNING_METHODS = ['last', 'full', 'new', 'chain-thaw']
FINETUNING_METRICS = ['acc', 'weighted']
